<?php

class Sparcksoft_Friendgate_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $post = Mage::app()->getRequest()->getQuery(); 

        $session_token = Mage::getSingleton('core/session')->getAccessToken();

        $result = array();
        if (!isset($post['access_token']))
            $result['status'] = 'error';
        else
        {
            $result['status'] = 'success';
            $result['access_token'] = $post['access_token'];
            Mage::getSingleton('core/session')->setAccessToken($post['access_token']);
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function tokenAction()
    {
        $result['access_token'] = Mage::getSingleton('core/session')->getAccessToken();
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
    
}

?>
