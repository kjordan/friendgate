<?php

/** @var $this Sparcksoft_Friendgate_Model_Resource_Mysql4_Setup */
$this->startSetup();

$table = $this->getTable('salesrule/rule');
$column = array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'unsigned'  => true,
    'nullable'  => false,
    'default'   => '0',
    'comment'   => 'Friend Gate'
);
$this->getConnection()->addColumn($table, 'friendgate', $column);

?>
