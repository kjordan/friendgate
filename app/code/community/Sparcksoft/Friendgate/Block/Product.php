<?php

class Sparcksoft_Friendgate_Block_Product extends Mage_Core_Block_Template
{
    protected $_authenticated = null;
    protected $_friendsThatLikeIt = null;
    protected $_facebookPage = null;

    function _construct()
    {
        // Cache lasts 1 second, so that new friend likes can get pulled in
        $this->addData(array(
            'cache_lifetime' => 1,
            'cache_tags'     => array(Mage_Catalog_Model_Product::CACHE_TAG),
            'cache_key'      => "FRIENDGATE_DISCOUNT_" . $this->getRule()->getId(),
        ));
    }

    public function getAppId()
    {
        return Mage::getStoreConfig('sparcksoft_friendgate/facebook_app/app_id');
    }

    public function getPageUrl()
    {
        /** @var $helper Sparcksoft_Friendgate_Helper_Data */
        $helper = Mage::helper('friendgate');
        return $helper->getPageUrl();
    }

    public function getLikesNeeded()
    {
        /** @var $helper Sparcksoft_Friendgate_Helper_Data */
        $helper = Mage::helper('friendgate');
        return $helper->getLikesNeeded();
    }

    public function isAuthenticated()
    {
        if (isset($this->_authenticated)) {
            return $this->_authenticated;
        }

        $this->_authenticated = $this->getFacebookPage()->isAuthenticated();
        return $this->_authenticated;
    }

    public function getFriendPhotoUrl($friend)
    {
        $url = "https://graph.facebook.com/" . $friend['uid'] . "/picture?width=100&height=100";
        return $url;
    }

    public function getFacebookPage()
    {
        if (isset($this->_facebookPage)) {
            return $this->_facebookPage;
        }

        /** @var $page Sparcksoft_Friendgate_Model_Facebook_Page */
        $page = Mage::getModel('friendgate/facebook_page');
        $page->load($this->getPageUrl());

        return ($this->_facebookPage = $page);
    }

    public function getFriendsThatLikeThePage()
    {
        if (isset($this->_friendsThatLikeIt)) {
            return $this->_friendsThatLikeIt;
        }

        $friends = $this->getFacebookPage()->getFriendsThatLikeIt();

        return ($this->_friendsThatLikeIt = $friends);
    }

    public function isUnlocked()
    {
        $friends = $this->getFriendsThatLikeThePage();
        $likesNeeded = $this->getLikesNeeded();

        if (count($friends) >= $likesNeeded) {
            return true;
        }

        return false;
    }

    public function getRule()
    {
        /** @var $rule Sparcksoft_Friendgate_Model_Salesrule_Rule */
        $rule = Mage::getModel('friendgate/salesrule_rule');
        $rule = $rule->getCollection()
            ->addFieldToFilter('friendgate', 1)
            ->getFirstItem();

        return $rule;
    }

    public function isEnabled()
    {
        /** @var $helper Sparcksoft_Friendgate_Helper_Data */
        $helper = Mage::helper('friendgate');

        if ($helper->isEnabled() && $this->getRule()->getId()) {
            return true;
        }

        return false;
    }

    public function getDiscountHeader()
    {
        return $this->__("Share the love.  Unlock the discount.");
    }
}
