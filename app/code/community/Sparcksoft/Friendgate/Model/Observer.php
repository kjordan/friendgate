<?php

class Sparcksoft_Friendgate_Model_Observer
{
    public function PromoSalesEdit(Varien_Event_Observer $observer)
    {
        $form = $observer->getForm();
        $fieldset = $form->getElement('base_fieldset');
        $fieldset->addField('friendgate', 'select', array(
            'label'     => Mage::helper('salesrule')->__('Friend Gate'),
            'title'     => Mage::helper('salesrule')->__('Friend Gate'),
            'name'      => 'friendgate',
            'options'    => array(
                '1' => Mage::helper('salesrule')->__('Active'),
                '0' => Mage::helper('salesrule')->__('Inactive'),
            ),
        ));
        $model = Mage::registry('current_promo_quote_rule');
        $form->setValues($model->getData());
    }

    public function coreCollectionAbstractLoadAfter(Varien_Event_Observer $observer)
    {
        $collection = $observer->getData('collection');
        if ($collection instanceof Mage_SalesRule_Model_Resource_Rule_Collection) {
            $this->_removeRulesIfGated($collection);
        }

        return $this;
    }

    /**
     * @param $rules Mage_SalesRule_Model_Resource_Rule_Collection
     */
    protected function _removeRulesIfGated($rules)
    {
        /** @var $rule Mage_SalesRule_Model_Rule */
        foreach ($rules as $rule) {
            if ($this->_ruleIsGated($rule)) {
                // $rules->removeItemByKey($rule->getId());
            }
        }
    }

    /**
     * @param $rule Mage_SalesRule_Model_Rule
     * @return $this
     */
    protected function _ruleIsGated($rule)
    {
        /** @var $helper Sparcksoft_Friendgate_Helper_Data */
        $helper = Mage::helper('friendgate');
        if (! $helper->isEnabled()) {
            return $this;
        }

        /** @var $page Sparcksoft_Friendgate_Model_Facebook_Page */
        $page = Mage::getModel('friendgate/facebook_page');
        $page->load($helper->getPageUrl());

        if (!$page->isAuthenticated()) {
            return $this;
        }

        $friends = $page->getFriendsThatLikeIt();
        if (count($friends) < $helper->getLikesNeeded()) {
            return true;
        }

        return false;
    }
}
