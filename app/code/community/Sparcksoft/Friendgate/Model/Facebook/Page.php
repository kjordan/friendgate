<?php

class Sparcksoft_Friendgate_Model_Facebook_Page extends Varien_Object
{
    const GRAPH_API_ENDPOINT = "https://graph.facebook.com";
    const FQL_API_ENDPOINT = "https://api.facebook.com";

    protected $_graphObjectId = null;

    protected $_client = null;

    protected $_friends = null;

    protected function getAccessToken()
    {
        $localDev = Mage::getStoreConfig('sparcksoft_friendgate/local_dev/enabled');
        if ($localDev) {
            $accessToken = Mage::getStoreConfig('sparcksoft_friendgate/local_dev/access_token');
        }
        else {
            $accessToken = Mage::getSingleton('core/session')->getData('access_token');
        }

        return $accessToken;
    }

    protected function _getHttpClient()
    {
        $client = new Varien_Http_Client();
        $client->setConfig(array('timeout' => 30))
            ->setHeaders('accept-encoding', '')
            ->setMethod(Zend_Http_Client::GET);

        return $client;
    }

    public function isAuthenticated()
    {
        try {
            $this->getFriendsThatLikeIt();
        } catch (Sparcksoft_Friendgate_Exception_Facebook_Authentication $e) {
            return false;
        }

        return true;
    }

    protected function _getGraphObjectId($pageUrl)
    {
        if (isset($this->_graphObjectId)) {
            return $this->_graphObjectId;
        }

        $uri = self::GRAPH_API_ENDPOINT;
        $parameter = array('ids' => $pageUrl);
        $request = $this->_getHttpClient()
            ->setUri($uri)
            ->setParameterGet($parameter)
            ->request();

        $json = $this->_formatFacebookJson($request->getBody());
        $pageInfo = json_decode($json, true);
        $pageInfo = array_values($pageInfo);

        if (!isset($pageInfo[0]) || !isset($pageInfo[0]['id'])) {
            throw new Exception("Wasn't able to find data on the Facebook page");
        }

        $this->_graphObjectId = $pageInfo[0]['id'];
        return $this->_graphObjectId;
    }

    public function load($url)
    {
        $this->_graphObjectId = $this->_getGraphObjectId($url);

        return $this;
    }

    public function getFriendsThatLikeIt()
    {
        if (isset($this->_friends)) {
            return $this->_friends;
        }

        // Setup the parameters for the FQL query
        $parameter = array (
            'access_token' => $this->getAccessToken(),
            'query' => "
                SELECT uid, page_id, type 
                FROM page_fan 
                WHERE (uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) OR uid = me())
                AND page_id = '" . $this->_graphObjectId . "'
            ",
            'format' => 'json',
        );

        $host = self::FQL_API_ENDPOINT;
        $request = $this->_getHttpClient()
            ->setUri("$host/method/fql.query")
            ->setParameterGet($parameter)
            ->request();

        $json = $this->_formatFacebookJson($request->getBody());
        $friends = json_decode($json, true);
        if (isset($friends['error_msg'])) {
            $this->_throwException($friends['error_msg']);
        }

        return ($this->_friends = $friends);
    }

    protected function _throwException($message)
    {
        if (strpos($message, 'valid signature') !== false) {
            throw new Sparcksoft_Friendgate_Exception_Facebook_Authentication("Not authenticated");
        } elseif (strpos($message, 'logged out') !== false) {
            throw new Sparcksoft_Friendgate_Exception_Facebook_Authentication("Access token looks like it's invalid now");
        } elseif (strpos($message, 'session was invalidated') !== false) {
            throw new Sparcksoft_Friendgate_Exception_Facebook_Authentication("Session was invalidated");
        } elseif (strpos($message, 'not authorized application') !== false) {
            throw new Sparcksoft_Friendgate_Exception_Facebook_Authentication("Application isn't authorized.");
        } else {
            throw new Exception($message);
        }
    }

    /**
     * Replacing the facebook page ID int with a string because json_decode
     * has a bug that converts it to scientific notation.
     *
     * @param $json
     * @return mixed
     */
    protected function _formatFacebookJson($json)
    {
        $json = preg_replace('/:(\d+),/', ':"${1}",', $json);
        return $json;
    }
}
