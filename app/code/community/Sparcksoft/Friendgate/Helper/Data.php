<?php

class Sparcksoft_Friendgate_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getPageUrl()
    {
        return Mage::getStoreConfig('sparcksoft_friendgate/settings/page_url');
    }

    public function getLikesNeeded()
    {
        return Mage::getStoreConfig('sparcksoft_friendgate/settings/num_likes');
    }

    public function isEnabled()
    {
        return Mage::getStoreConfig('sparcksoft_friendgate/settings/enabled');
    }
}